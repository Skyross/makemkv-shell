import enum
import itertools
import pathlib
import re

import asyncclick as click
import structlog
from aiomultiprocess import Pool

from makemkv_shell.utils import run


@enum.unique
class InputType(str, enum.Enum):
    BDMV = "BDMV"
    DVD = "DVD"


TITLES = (
    (
        re.compile(r"^File \d+.(mpls|m2ts) was added as title #(?P<TitleID>\d+)"),
        InputType.BDMV,
    ),
    (re.compile(r"^Title #(?P<TitleID>\d+) was added"), InputType.DVD),
)

logger = structlog.get_logger(__name__)


def get_default_executable() -> pathlib.Path:
    for path in (
        pathlib.Path("/Applications/MakeMKV.app/Contents/MacOS/makemkvcon"),
        pathlib.Path("C:\\Program Files (x86)\\MakeMKV\\makemkvcon64.exe"),
        pathlib.Path("C:\\Program Files (x86)\\MakeMKV\\makemkvcon.exe"),
    ):
        if path.exists():
            return path


@click.command()
@click.option(
    "--executable",
    "-e",
    type=click.Path(exists=True, path_type=pathlib.Path),
    default=get_default_executable,
    help="Path to MakeMKV executable",
)
@click.option(
    "--input",
    "-i",
    "input_",
    type=click.Path(exists=True, path_type=pathlib.Path),
    prompt="Input",
    help="Filepath to input.",
)
@click.option(
    "--output",
    "-o",
    type=click.Path(exists=True, path_type=pathlib.Path),
    prompt="Output",
    help="Filepath to output.",
)
async def cli(
    executable: pathlib.Path,
    input_: pathlib.Path,
    output: pathlib.Path,
) -> None:
    title_ids = []

    stdout, stderr = await run(
        executable, "info", f"file:{input_}", process_label="Info"
    )

    detected_input_type = None
    for line in itertools.chain(stdout.splitlines(), stderr.splitlines()):
        for pattern, input_type in TITLES:
            if match := pattern.match(line):
                title_id = match.group("TitleID")
                title_ids.append(int(title_id))
                detected_input_type = input_type
                break

    if not title_ids:
        return

    async with Pool() as pool:
        task_ids = []

        for title_id in title_ids:
            normalized_title_id = (
                str(title_id)
                if detected_input_type == InputType.BDMV
                else str(title_id - 1)
            )

            task_ids.append(
                pool.queue_work(
                    run,
                    (
                        executable,
                        "mkv",
                        f"file:{input_}",
                        normalized_title_id,
                        str(output.absolute()),
                    ),
                    {"process_label": f"Title #{title_id}"},
                )
            )

        await pool.results(task_ids)


def main() -> None:
    cli(_anyio_backend="asyncio")


if __name__ == "__main__":
    main()
