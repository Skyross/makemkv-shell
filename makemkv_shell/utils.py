import asyncio
import pathlib
from typing import Sequence, Union, Optional, Tuple

import structlog


async def run(
    program,
    *args: Sequence[Union[pathlib.Path, str]],
    process_label: Optional[str] = None,
) -> Tuple[str, str]:
    """Capture output (stdout and stderr) while running external command."""
    logger = structlog.get_logger(
        __name__,
        **{"process_label": process_label} if process_label is not None else {},
    )

    process = await asyncio.create_subprocess_exec(
        program,
        *args,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )

    stdout, stderr = await process.communicate()
    result, error = stdout.decode(), stderr.decode()

    for line in result.splitlines():
        logger.info(line)
    for line in error.splitlines():
        logger.error(line)

    logger.info(
        f"Exited",
        command=" ".join(map(str, (program,) + args)),
        returncode=process.returncode,
    )

    return result, error
